package controller;

import java.util.ArrayList;

import lookup.LookupClient;
import server.Server;
import view.MainWindow;

/**
 *
 * Projeto da disciplina de Tópicos Avançados em Sistemas Distribuídos (if749)
 * 
 * APLICAÇÃO DE TESTE
 *
 * @author Marcelo Nascimento Oliveira
 *
 */
public class Main {
	public static final String LOOKUP_ADRRESS = "localhost";
	public static final int LOOKUP_PORT = 6000;
	private static MainWindow window;
	private static ArrayList<GameProxy> remoteGames;
	private static ArrayList<Game> localGames;
	private static int objectPort;

	public static void main(String[] args) {
		LookupClient.getInstance().setLookupAddress(Main.LOOKUP_ADRRESS);
		LookupClient.getInstance().setLookupPort(Main.LOOKUP_PORT);
		Main.objectPort = Server.getInstance().start(0);
		Main.remoteGames = new ArrayList<GameProxy>(1);
		Main.localGames = new ArrayList<Game>(1);
		Main.window = new MainWindow();
		Main.window.alignAndShow();
	}

	public static void addRemote(GameProxy game) {
		Main.remoteGames.add(game);
	}
	
	public static void addLocal(Game game) {
		Main.localGames.add(game);
	}
	
	public static int getObjectPort() {
		return Main.objectPort;
	}
	
}
