package controller;

import java.util.Random;

import lookup.AOR;
import view.GameUI;
import client.Requestor;
import common.Request;
import common.interfaces.CallbackObject;
import controller.callback.JoinCallback;
import controller.callback.MoveCallback;
import controller.callback.TokenCallback;
import controller.interfaces.GameInterface;
import controller.listeners.GameCellListener;

public class GameProxy implements GameInterface {
	private AOR aor;
	private Long remotePlayerId;
	private CallbackObject joinCallback;
	private CallbackObject tokenCallback;
	private CallbackObject moveCallback;
	private GameUI gameUI;
	
	public GameProxy(AOR aor, String partyName) {
		this.aor = aor;
		this.remotePlayerId = 
				(new Random(System.currentTimeMillis())).nextLong();
		this.gameUI = new GameUI(partyName);
		this.gameUI.addListener(new GameCellListener(this));
		this.joinCallback = new JoinCallback(this);
		this.tokenCallback = new TokenCallback(this);
		this.moveCallback = new MoveCallback(this);
	}

	public Integer[] join(Long id) {
		Request request = new Request(this.aor, "join", new Object[] {id});
		Requestor.getInstance().sendInvocation(request, joinCallback);
		return null;
	}

	public Integer[] getToken() {
		Request request = new Request(this.aor, "getToken", new Object[] {});
		Requestor.getInstance().sendInvocation(request, tokenCallback);
		return null;
	}

	public Integer[] makeMove(Long id, Integer player, Integer cell) {
		Request request = new Request(this.aor, "makeMove", 
				new Object[] {id, player, cell});
		Requestor.getInstance().sendInvocation(request, moveCallback);
		return null;
	}

	public Long getRemotePlayerId() {
		return this.remotePlayerId;
	}

	public GameUI getGameUI() {
		return gameUI;
	}

	@Override
	public void leave(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getLocalPlayerId() {
		return null;
	}

}
