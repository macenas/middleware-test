package controller.interfaces;

import view.GameUI;

public interface GameInterface {
	public Integer[] join(Long id);
	public Integer[] getToken();
	public Integer[] makeMove(Long id, Integer player, Integer cell);
	public void leave(Long id);
	public Long getLocalPlayerId();
	public Long getRemotePlayerId();
	public GameUI getGameUI();
}
