package controller;

public class Utils {

	public static Boolean isFinished(Integer[] gameboard, Integer player) {
		Boolean result = false;
		Integer c0 = gameboard[0];
		Integer c1 = gameboard[1];
		Integer c2 = gameboard[2];
		Integer c3 = gameboard[3];
		Integer c4 = gameboard[4];
		Integer c5 = gameboard[5];
		Integer c6 = gameboard[6];
		Integer c7 = gameboard[7];
		Integer c8 = gameboard[8];
		
		if (c0.equals(player) && c1.equals(player) && c2.equals(player) ||
			c3.equals(player) && c4.equals(player) && c5.equals(player) ||
			c6.equals(player) && c7.equals(player) && c8.equals(player) ||
			c0.equals(player) && c3.equals(player) && c6.equals(player) ||
			c1.equals(player) && c4.equals(player) && c7.equals(player) ||
			c2.equals(player) && c5.equals(player) && c7.equals(player) ||
			c0.equals(player) && c4.equals(player) && c8.equals(player) ||
			c2.equals(player) && c4.equals(player) && c6.equals(player))
			result = true;
		
		return result;
	}
	
	public static Boolean isEmpty(Integer[] gameboard) {
		Boolean result = true;
		
		
		for (int i = 0; i < 9; i++) {
			
			if (!gameboard[i].equals(Game.NONE)) {
				result = false;
				break;
			}
			
		}
		
		return result;
	}
	
}
