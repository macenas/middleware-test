package controller.listeners;

import javax.swing.DefaultListModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import lookup.LookupClient;
import view.MainWindow;

public class MainTabbedPaneListener implements ChangeListener {
	private MainWindow window;
	
	public MainTabbedPaneListener(MainWindow window) {
		this.window = window;
	}
	
	@Override
	public void stateChanged(ChangeEvent event) {
		
		if (window.getMainTabbedPane().getSelectedIndex() == 1) {
			String[] partiesList = LookupClient.getInstance().list();
			((DefaultListModel<String>) window.getPartiesList().getModel()).
				clear();
			
			for (int i = 0; i < partiesList.length; i++) {
				((DefaultListModel<String>) window.getPartiesList().getModel()).
					addElement(partiesList[i]);
			}
			
		}
		
	}

}
