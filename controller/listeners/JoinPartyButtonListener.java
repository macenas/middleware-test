package controller.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import lookup.AOR;
import lookup.LookupClient;
import view.MainWindow;
import controller.GameProxy;

public class JoinPartyButtonListener implements ActionListener {
	private MainWindow window;
	
	public JoinPartyButtonListener(MainWindow window) {
		this.window = window;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		String partyName = this.window.getPartiesList().getSelectedValue();
		AOR aor = LookupClient.getInstance().getAOR(partyName);
		GameProxy game = new GameProxy(aor, partyName);
		game.join(game.getRemotePlayerId());
	}

}
