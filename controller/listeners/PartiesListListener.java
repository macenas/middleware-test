package controller.listeners;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import view.MainWindow;

public class PartiesListListener implements ListSelectionListener {
	private MainWindow window;
	
	public PartiesListListener(MainWindow window) {
		this.window = window;
	}
	
	@Override
	public void valueChanged(ListSelectionEvent event) {
		
		if (window.getPartiesList().getSelectedIndex() != -1)
			window.getJoinPartyButton().setEnabled(true);
		else
			window.getJoinPartyButton().setEnabled(false);
			
	}

}
