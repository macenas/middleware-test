package controller.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import view.MainWindow;

public class PartiesNameFieldListener implements KeyListener {
	private MainWindow window;
	
	public PartiesNameFieldListener(MainWindow window) {
		this.window = window;
	}

	@Override
	public void keyPressed(KeyEvent event) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		if (window.getPartyNameField().getText().equals(""))
			window.getCreatePartyButton().setEnabled(false);
		else
			window.getCreatePartyButton().setEnabled(true);
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
		
	}

}
