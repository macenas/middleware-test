package controller.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import lookup.LookupClient;
import view.MainWindow;
import view.Utils;
import controller.Game;
import controller.Main;

public class CreatePartyButtonListener implements ActionListener {
	private MainWindow window;
	
	public CreatePartyButtonListener(MainWindow window) {
		this.window = window;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		String partyName = window.getPartyNameField().getText();
		Game game = new Game(partyName);
		String response = LookupClient.getInstance().register(partyName,
				Main.getObjectPort(), game);

		if (response.equals("Registrado."))
			Utils.alignAndShow(game.getGameUI());
		else
			JOptionPane.showMessageDialog(window, response);
		
	}

}
