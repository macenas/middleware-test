package controller.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;

import controller.Game;
import controller.interfaces.GameInterface;

public class GameCellListener implements MouseListener {
	private GameInterface game;
	private Long id;
	private Integer player;
	
	public GameCellListener(GameInterface game) {
		this.game = game;
		
		if (this.game.getClass().getName().equals("controller.Game")) {
			this.id = this.game.getLocalPlayerId();
			this.player = Game.LOCAL;
		} else if (this.game.getClass().getName().
				equals("controller.GameProxy")) {
			this.id = this.game.getRemotePlayerId();
			this.player = Game.REMOTE;
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		
		if (((JLabel) event.getSource()).isEnabled()) {
			int cell = Integer.valueOf(((JLabel) event.getSource()).getName());
			this.game.makeMove(this.id, this.player, cell);
			
			synchronized (this.game) {
				this.game.notify();
			}
			
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent event) {
	}

	@Override
	public void mouseExited(MouseEvent event) {
	}

	@Override
	public void mousePressed(MouseEvent event) {
	}

	@Override
	public void mouseReleased(MouseEvent event) {
	}

}
