package controller;

import java.util.Random;

import controller.interfaces.GameInterface;
import controller.listeners.GameCellListener;
import view.GameUI;

public class Game implements GameInterface {
	public static final Integer LOCAL = 0;
	public static final Integer REMOTE = 1;
	public static final Integer NONE = -1;
	private Long localPlayerId;
	private Long remotePlayerId;
	private Integer turn;
	private Integer[] gameboard;
	private GameUI gameUI;
	
	public Game(String partyName) {
		this.clear(Game.LOCAL);
		this.remotePlayerId = (long) -1;
		this.localPlayerId = 
				(new Random(System.currentTimeMillis())).nextLong();
		this.gameUI = new GameUI(partyName);
		this.gameUI.addListener(new GameCellListener(this));
	}
	
	public synchronized Integer[] join(Long id) {
		Integer[] result = this.gameboard;
		
		if (this.remotePlayerId == -1)
			this.remotePlayerId = id;
		else
			result = null;
		
		return result;
	}
	
	public synchronized Integer[] getToken() {
		
		if (this.turn == Game.LOCAL) {
			this.gameUI.refresh(this.gameboard, true, GameUI.MOVE);
		
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	
		return this.gameboard;
	}
	
	public synchronized Integer[] makeMove(Long id, Integer player,
			Integer cell) {
		
		if (id.equals(this.remotePlayerId) || id.equals(this.localPlayerId))
			this.gameboard[cell] = player;
		
		if (Utils.isFinished(this.gameboard, Game.LOCAL)) {
			this.clear(Game.LOCAL);
			this.gameUI.incrementLocalScore();
			this.gameUI.refresh(this.gameboard, true, GameUI.MOVE);
		} else if (Utils.isFinished(this.gameboard, Game.REMOTE)) {
			this.clear(Game.REMOTE);
			this.gameUI.incrementRemoteScore();
			this.gameUI.refresh(this.gameboard, false, GameUI.WAIT_MOVE);
		} else if (player.equals(Game.LOCAL)) {
			this.turn = Game.REMOTE;
			this.gameUI.refresh(this.gameboard, false, GameUI.WAIT_MOVE);
		} else if (player.equals(Game.REMOTE))
			this.turn = Game.LOCAL;
			
		return this.gameboard;
	}

	public synchronized void leave(Long id) {
		
		if (id.equals(this.remotePlayerId)) {
			this.clear(Game.LOCAL);
			this.remotePlayerId = (long) -1;
		}
		
	}
	
	private synchronized void clear(Integer turn) {
		this.turn = turn;
		this.gameboard = new Integer[] {Game.NONE, Game.NONE, Game.NONE,
										Game.NONE, Game.NONE, Game.NONE,
										Game.NONE, Game.NONE, Game.NONE};
	}
	
	public Long getLocalPlayerId() {
		return this.localPlayerId;
	}
	
	public Long getRemotePlayerId() {
		return this.remotePlayerId;
	}
	
	public GameUI getGameUI() {
		return gameUI;
	}

	public void setGameUI(GameUI gameUI) {
		this.gameUI = gameUI;
	}

}