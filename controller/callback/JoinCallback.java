package controller.callback;

import javax.swing.JOptionPane;

import view.GameUI;
import view.Utils;
import common.interfaces.CallbackObject;
import controller.GameProxy;
import controller.Main;

public class JoinCallback implements CallbackObject {
	private GameProxy game;
	
	public JoinCallback(GameProxy game) {
		this.game = game;
	}
	
	@Override
	public void run(Object result) {
		
		if (((Integer[]) result) != null) {
			Main.addRemote(this.game);
			Utils.alignAndShow(this.game.getGameUI());
			this.game.getGameUI().refresh(
					(Integer[]) result, false, GameUI.WAIT_MOVE);
			this.game.getToken();
		} else
			JOptionPane.showMessageDialog(null, "Partida cheia.");
		
	}

}
