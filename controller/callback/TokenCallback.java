package controller.callback;

import view.GameUI;
import common.interfaces.CallbackObject;
import controller.GameProxy;
import controller.Utils;

public class TokenCallback implements CallbackObject {
	private GameProxy game;
	
	public TokenCallback(GameProxy game) {
		this.game = game;
	}
	
	@Override
	public void run(Object result) {
		
		if (Utils.isEmpty((Integer[]) result)) {
			this.game.getGameUI().incrementRemoteScore();
			this.game.getGameUI().refresh(
					(Integer[]) result, false, GameUI.WAIT_MOVE);
			this.game.getToken();
		} else {
			this.game.getGameUI().refresh(
					(Integer[]) result, true, GameUI.MOVE);
		
			try {
				
				synchronized (this.game) {
					this.game.wait();
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
		
	}

}
