package controller.callback;

import view.GameUI;
import common.interfaces.CallbackObject;
import controller.GameProxy;
import controller.Utils;

public class MoveCallback implements CallbackObject {
	private GameProxy game;
	
	public MoveCallback(GameProxy game) {
		this.game = game;
	}
	
	@Override
	public void run(Object result) {

		if (Utils.isEmpty((Integer[]) result)) {
			this.game.getGameUI().incrementLocalScore();
			this.game.getGameUI().refresh(
					((Integer[]) result), true, GameUI.MOVE);
		} else {
			this.game.getGameUI().refresh(
					((Integer[]) result), false, GameUI.WAIT_MOVE);
			this.game.getToken();
		}
		
	}

}
