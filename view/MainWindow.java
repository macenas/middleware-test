package view;
	
import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controller.listeners.CreatePartyButtonListener;
import controller.listeners.JoinPartyButtonListener;
import controller.listeners.MainTabbedPaneListener;
import controller.listeners.PartiesListListener;
import controller.listeners.PartiesNameFieldListener;
	
public class MainWindow extends JFrame {
	private static final long serialVersionUID = 4397636240550069720L;
	private JTabbedPane mainTabbedPane;
	private JPanel createPanel;
	private JPanel joinPanel;
	private JPanel partyNamePanel;
	private JPanel createButtonsPanel;
	private JPanel joinButtonsPanel;
	private JTextField partyNameField;
	private JButton createPartyButton;
	private JButton joinPartyButton;
	private DefaultListModel<String> partiesListModel;
	private JList<String> partiesList;
	private JScrollPane partiesScrollPane;
	
	public MainWindow() {
		super("Projeto de TASD - Aplicação de teste");
		// Sets "look and fell" to system default.
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Creates UI elements.
		this.mainTabbedPane = new JTabbedPane(JTabbedPane.TOP,
				JTabbedPane.SCROLL_TAB_LAYOUT);
		this.createPanel = new JPanel();
		this.joinPanel = new JPanel();
		this.partyNamePanel = new JPanel();
		this.createButtonsPanel = new JPanel();
		this.joinButtonsPanel = new JPanel();
		this.partyNameField = new JTextField();
		this.createPartyButton = new JButton("Criar");
		this.joinPartyButton = new JButton("Juntar-se");
		this.partiesListModel = new DefaultListModel<String>();
		this.partiesList = new JList<String>(this.partiesListModel);
		this.partiesScrollPane = new JScrollPane(this.partiesList);

		// Adjusts UI elements.
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(400, 600);
		this.createPanel.setLayout(new BorderLayout());
		this.joinPanel.setLayout(new BorderLayout());
		this.partyNameField.setColumns(25);
		this.createPartyButton.setEnabled(false);
		this.joinPartyButton.setEnabled(false);
		this.partiesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		// Adds listeners.
		this.createPartyButton.addActionListener(
				new CreatePartyButtonListener(this));
		this.joinPartyButton.addActionListener(
				new JoinPartyButtonListener(this));
		this.partyNameField.addKeyListener(new PartiesNameFieldListener(this));
		this.partiesList.addListSelectionListener(
				new PartiesListListener(this));
		this.mainTabbedPane.addChangeListener(new MainTabbedPaneListener(this));

		// Adds UI elements.
		this.add(this.mainTabbedPane);
		this.mainTabbedPane.addTab("Criar", this.createPanel);
		this.mainTabbedPane.addTab("Juntar-se", this.joinPanel);
		this.partyNamePanel.add(new JLabel("Nome da partida: "));
		this.partyNamePanel.add(this.partyNameField);
		this.createButtonsPanel.add(this.createPartyButton);
		this.joinButtonsPanel.add(this.joinPartyButton);
		this.createPanel.add(this.partyNamePanel);
		this.createPanel.add(this.createButtonsPanel, BorderLayout.SOUTH);
		this.joinPanel.add(this.partiesScrollPane, BorderLayout.CENTER);
		this.joinPanel.add(joinButtonsPanel, BorderLayout.SOUTH);
	}
	
	public void alignAndShow() {
		Utils.alignAndShow(this);
	}
	
	public JList<String> getPartiesList() {
		return this.partiesList;
	}
	
	public JTextField getPartyNameField() {
		return this.partyNameField;
	}
	
	public JButton getCreatePartyButton() {
		return this.createPartyButton;
	}
	
	public JButton getJoinPartyButton() {
		return this.joinPartyButton;
	}
	
	public JTabbedPane getMainTabbedPane() {
		return this.mainTabbedPane;
	}
	
}
