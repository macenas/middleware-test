package view;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import controller.listeners.GameCellListener;

public class GameUI extends JFrame {
	private static final long serialVersionUID = 5090138524410000797L;
	public static final int WAIT_CONNECTION = 0;
	public static final int WAIT_MOVE = 1;
	public static final int MOVE = 2;
	private String[] messages;
	private ImageIcon iconNone;
	private ImageIcon iconX; 
	private ImageIcon iconO;
	private JLabel[] cells;
	private JTextArea message;
	private JTextArea score;
	private int localScore;
	private int remoteScore;
	
	public GameUI(String partyName) {
		this.setResizable(false);
		this.setLayout(new GridLayout(4, 3));
		this.setTitle(partyName);
		this.messages = new String[3];
		this.messages[GameUI.WAIT_CONNECTION] = "Aguardando conexão...";
		this.messages[GameUI.WAIT_MOVE] = "Aguardando jogada do oponente...";
		this.messages[GameUI.MOVE] = "Sua vez!";
		this.iconNone = new ImageIcon(
				this.getClass().getResource("/view/img/none.png"));
		this.iconX = new ImageIcon(
				this.getClass().getResource("/view/img/x.png"));
		this.iconO = new ImageIcon(
				this.getClass().getResource("/view/img/o.png"));
		this.cells = new JLabel[9];

		for (int i = 0; i < 9; i++) {
			this.cells[i] = new JLabel();
			this.cells[i].setIcon(iconNone);
			this.cells[i].setName(Integer.toString(i));
			this.cells[i].setBorder(
					BorderFactory.createEmptyBorder(2, 2, 2, 2));
			this.cells[i].setEnabled(false);
			this.add(this.cells[i]);
		}

		JLabel model = new JLabel();
		this.message = new JTextArea(this.messages[GameUI.WAIT_CONNECTION]);
		this.message.setEnabled(false);;
		this.message.setLineWrap(true);
		this.message.setWrapStyleWord(true);
		this.message.setFont(model.getFont().deriveFont(14));
		this.message.setDisabledTextColor(model.getForeground());
		this.message.setBackground(this.getBackground());
		this.score = new JTextArea("0 x 0");
		this.score.setEnabled(false);;
		this.score.setLineWrap(true);
		this.score.setWrapStyleWord(true);
		this.score.setFont(model.getFont().deriveFont(14));
		this.score.setDisabledTextColor(model.getForeground());
		this.score.setBackground(this.getBackground());
		this.add(this.message);
		this.add(this.score);
		this.pack();
	}
	
	public void refresh(Integer[] gameboard, Boolean enabled, int message) {
		
		for (int i = 0; i < 9; i++) {
			
			if (gameboard[i].equals(0)) {
				cells[i].setIcon(this.iconO);
				cells[i].setEnabled(false);
			} else if (gameboard[i].equals(1)) {
				cells[i].setIcon(this.iconX);
				cells[i].setEnabled(false);
			} else if (gameboard[i].equals(-1)) {
				cells[i].setIcon(this.iconNone);
				cells[i].setEnabled(enabled);
			}
			
		}
		
		this.message.setText(this.messages[message]);
		this.score.setText(this.localScore + " x " + this.remoteScore);
	}

	public void addListener(GameCellListener listener) {
		
		for (int i = 0; i < 9; i++)
			this.cells[i].addMouseListener(listener);

	}
	
	public void incrementLocalScore() {
		this.localScore++;
	}
	
	public void incrementRemoteScore() {
		this.remoteScore++;
	}
	
}